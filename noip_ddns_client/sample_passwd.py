#! /usr/bin/env python

details = {}
details["username"] = "my.noip.com username"
details["password"] = "password, plaintext please"
details["domain"] = {"ddns.net": ["myhost"]}
details["endpoint"] = "https://dynupdate.no-ip.com/nic/update"
