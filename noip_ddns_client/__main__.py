#! /usr/bin/env python3

from .ddns_client import set_ddns, get_global_ip
from .passwd import details


def run():
    print("Getting the global IP")
    details["ip"] = get_global_ip()
    set_ddns(details)


if __name__ == "__main__":
    run()
