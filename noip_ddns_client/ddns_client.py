#! /usr/bin/env python3

from __future__ import unicode_literals
import base64
import requests
import time


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


def get_global_ip():
    endpoint = "https://ifconfig.co/"
    response = requests.request("GET",
                                endpoint,
                                headers={'User-Agent': 'curl'})
    return response.text.strip()


@static_vars(fatal_error="")
def noip_ddns_api(basic_auth_str, url, ip):
    if noip_ddns_api.fatal_error:
        return

    endpoint = "https://dynupdate.no-ip.com/nic/update"

    querystring = {"hostname": url, "myip": ip}
    print(querystring)

    payload = ""
    headers = {
        'User-Agent': "Kunal Python/v0.9 kunal.tyagi@rakuten.com",
        'Authorization': "Basic " + basic_auth_str
    }

    response = requests.request(
        "GET",
        endpoint,
        data=payload,
        headers=headers,
        params=querystring)

    status = response.text.strip().split()

    if status[0] in ["good", "nochg"]:
        print("\tSuccess. {} is now {}. Status: {}".format(
            status[1], url, status[0]))
    elif status[0] is "nohost":
        print("\tFailure. Check spelling of {} due to: ", status)
        time.sleep(0.5)
        noip_ddns_api.fatal_error = False
    elif status[0] in ["badauth", "badagent", "!donator", "abuse", "911"]:
        print("\tSlowing down due to: ", status)
        time.sleep(1)
        noip_ddns_api.fatal_error = True
        return


def basic_auth(username, password):
    string = username + ":" + password
    return base64.b64encode(string.encode("ascii")).decode("ascii")


def set_ddns(details):
    basic_auth_str = basic_auth(details["username"], details["password"])
    # print(details["username"], basic_auth_str)
    for domain in details["domain"]:
        for hostname in details["domain"][domain]:
            url = hostname + "." + domain

            noip_ddns_api(basic_auth_str, url, details["ip"])
            print("")


def run():
    print("Getting the global IP")
    details["ip"] = get_global_ip()
    set_ddns(details)


if __name__ == "__main__":
    run()
