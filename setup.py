from setuptools import setup

setup(name='noip_ddns_client',
      version='0.1',
      description='Python client for noip ddns service',
      url='http://gitlab.com/kunaltyagi/noip_ddns_client',
      author='Kunal Tyagi',
      author_email='kunal.tyagi@rakuten.com',
      license='MPL2',
      packages=['noip_ddns_client'],
      install_requires=[
          'requests',
      ],
      zip_safe=False)
