## What's this

If you're looking for a URL for your devices, this is the right place.

You'll need
* python 2/3
* requests, base64 and time packages
* account on noip with a registered hostname

## How to use it

1. enter noip_ddns_client/
2. copy sample_passwd.py to passwd.py
3. enter the correct details
4. ensure the dependencies are installed
5. python ddns_client.py
